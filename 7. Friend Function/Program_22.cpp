

// Create a class co-ordinate containing x,y and z private variable. Perform operation for incrementing adding and comparing object(s) by overloading ++,+= and == operators respectively.


#include<iostream>
#include<stdio.h>
using namespace std;
class Coordinate{
    int x,y,z;
    public:
        int read(){
            cout<<"Enter the coordinates:";
            cin>>x>>y>>z;
        }
        void operator ++(){
            x++;
            y++;
            z++;
        }
        int operator +=(Coordinate m){
            x+= m.x;
            y+= m.y;
            z+= m.z;
        }
        int operator ==(Coordinate m){
            if(x==m.x && y==m.y && z==m.z)
            return 1;
            else
            return 0;
        }
        int display(){
            cout<<"Coordinate=("<<x<<","<<y<<","<<z<<")\n";
        }
};
int main(){
    Coordinate c1;
    Coordinate c2;
    c1.read();
    c2.read();
    if(c1==c2)
    cout<<"Same point\n";
    else
    cout<<"Different points\n";
    ++c1;
    c1.display();
    c1+=c2;
    c1.display();
}


