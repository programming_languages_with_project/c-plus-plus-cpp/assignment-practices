

// WAP to operate overloading to overload the'<<'and'>>'operator for class'TIME'.The data member of TIME class are HH,MM,SS. 
// Write necessary constructors.Create'n'objects of TIME class and display them in a suitable format.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Time{
        int hh,mm,ss;
        public:
            int getdata(){
                cout<<"Enter current time and hours,minutes and seconds:";
                cin>>hh>mm>>ss;
            }
            int putdata(){
                cout<<"Time:"<<hh<<":"<<mm<<":"<<ss<<endl;
            }
            int operator ++(){
                ss++;
                if(ss==60){
                    ss=0;
                    mm++;
                    if(mm==60){
                        mm=0;
                        hh++;
                        if(hh==24)
                        hh=0;
                    }
                }
            }
            int operator --(){
                ss--;
                if(ss==0){
                    ss=59;
                    mm--;
                    if(mm==0){
                        mm=59;
                        hh--;
                        if(hh==00)
                        hh=23;
                    }
                }
            }
    };
    int main(){
        Time t;
        t.getdata();
        t.putdata();
        t++;
        cout<<"After one second:\n";
        t.putdata();
    }


