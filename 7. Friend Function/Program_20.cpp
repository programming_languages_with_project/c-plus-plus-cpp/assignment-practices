

// Write s class to create array object with given size. Overload the operator * to multiply the elements of an array object with scalar value.
// Use them in main() to perform operation like s*a1,where s is scalar value and a1 is array object.



    #include<iostream>
    #include<stdio.h>
    using namespace std;
    class Array{
        int a;
        public:
            Array(){
                cout<<"Enter a value:";
                cin>>a;
            }
            int operator*(int x){
                a=a*x;
            }
            int display(){
                cout<<"a="<<a<<endl;
            }
    };
    int main(){
        int x,i,g;
        Array a[5];
        cout<<"Enter the scalar multiplier:";
        cin>>x;
        for(int i=0;i<=4;i++){
            a[i]*x;
        }
        for(int i=0;i<=4;i++){
            a[i].display();
        }
    }

