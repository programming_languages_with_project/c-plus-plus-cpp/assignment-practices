

// WAP to Overload operator +,+=,++ on complex number. ++should increment real and imaginary part by 1.


#include<iostream>
#include<stdio.h>
using namespace std;

class Complex{
    int x,y;
    public:
        int read(){
            cout<<"Enter the real and imaginary part of a complex number :";
            cin>>x>>y;
        }
        int operator +=(Complex c){
            x = x + c.x;
            y = y + c.y;
        }
        Complex operator +(Complex c){
            Complex c1;
            c1.x = x + c.x;
            c1.y = y + c.y;
            return c1;
        }
        int operator ++(){
            x++;
            y++;
        }
        int display(){
            if(y<0)
            cout<<x<<y<<"i";
            else
            cout<<x<<" +i"<<y<<endl;
        }
};
int main(){
    Complex c1;
    Complex c2;
    Complex c3;
    c1.read();
    c2.read();
    c1+=c2;
    
    cout<<"After adding the two complex number,result is: ";
    c1.display();
    c3 = c1+c2;
    
    cout<<"After adding the result of previous addition with the second number,result is:";
    c3.display();
    c3++;

    cout<<"After incrementing the above result,we get:";
    c3.display();
}



