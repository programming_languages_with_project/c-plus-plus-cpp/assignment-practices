

/// WAP to add two complex number using a friend function to add the complex numbers.

#include<iostream>
#include<stdio.h>
using namespace std;

class Complex{
    int x,y;
    public:
        int read(){
            cout<<"Enter the real and Imaginary part of a complex number :";
            cin>>x>>y;
        }
        friend Complex add (Complex c1,Complex c2);
        int display(){
            if(y<0)
            cout<<x<<y<<"i";
            else
            cout<<x<<" + i"<<y;
        }
};
Complex add(Complex c1,Complex c2){
    Complex c;
    c.x = c1.x + c2.x;
    c.y = c1.y + c2.y;
    return c;
}
int main(){
    Complex c1;
    Complex c2;
    Complex c3;
    c1.read();
    c2.read();
    c3=add(c1,c2);
    c3.display();
}


