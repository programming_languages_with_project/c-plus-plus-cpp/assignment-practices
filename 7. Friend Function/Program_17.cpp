

// Define a class complex with real and imaginary as two data member with default & parameterized constructor,function to initialize and display data of class.It should overload the +operator to add tow complex objects.
// Write a complete C++ program to demonstrate use of complex class.


#include<iostream>
#include<stdio.h>
using namespace std;
class Complex{
    int x,y;
    public:
        Complex(){
            x =y =5;
        }
        Complex(int a,int b){
            x =a;
            y =b;
        }
        int get_data(){
            cout<<"Enter the real and imaginary part of Complex number:";
            cin>>x>>y;
        }
        int display(){
            if(y>=0)
            cout<<x<<"+i"<<y<<endl;
            else
            cout<<x<<y<<"i"<<endl;
        }
        Complex operator +(Complex a){
            Complex z;
            z.x = x + a.x;
            z.y = y + a.y;
            return z;
        }
};
int main(){
    Complex c1,c2,c3;
    c1.get_data();
    c2.get_data(); 
    c3 = c1 + c2;
    cout<<"SUM =";
    c3.display();
}






