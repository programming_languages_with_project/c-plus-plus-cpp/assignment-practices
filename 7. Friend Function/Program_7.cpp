

/// Write pure virtual function convert liters to milli liters ,grams to kilograms,dollar to rupee.Assume 1 doller is 45 rupees.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Conversion{
        public:
        virtual int convert(){

        }
    };
    class Weight:public Conversion{
            protected:
                long int gm,kg;
                public:
                    int read(){
                        cout<<"Enter weight in kilograms:";
                        cin>>kg;
                    }
                    int convert(){
                        gm = kg*1000;
                    }
                    int display(){
                        cout<<"The weight in grams is"<<gm;
                    }
        };
        class Volume:public Conversion{
                protected:
                    long int ml,lt;
                    public:
                        int read(){
                            cout<<"Enter the volumn in liters:";
                            cin>>lt;
                        }
                        int convert(){
                            ml = lt*1000;
                        }
                        int display(){
                            cout<<"The volumn in mililiters is"<<ml;
                        }
            };
        class Currency:public Conversion{
                protected:
                    long int dollar,rs;
                    public:
                        int read(){
                        cout<<"Enter amount in Dollar:";
                        cin>>dollar;
                    }
                    int convert(){
                        rs=dollar*45;
                    }
                    int display(){
                        cout<<"The amount in rupees is"<<rs;
                    }
            };

        void main(){
            int choice;
            cout<<"1. Covert Kilogram to Gram \n 2. Convert Liters to Muli-Liter\n 3. Convert Dollar to Rupees\n Enter your choice :";
            cin>>choice;
            switch (choice)
            {
            case 1: Weight w;
                    w.read();
                    w.convert();
                    w.display();
                    break;
            case 2: Volume v;
                    v.read();
                    v.convert();
                    v.display();
                    break;                    
            case 3: Currency c;
                    c.read();
                    c.convert();
                    c.display();
                    break;            
            default:cout<<"Invalid Choice";
            }
        }