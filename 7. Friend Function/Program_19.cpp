

/* Define a circle class with radius as data member,necessary constructor and member function to compute area of circle. 
   Class should overload the == operator to compare two circle objects whether they are equal in radius.Demonstrate its use in main().*/


    #include<iostream>
    #include<stdio.h>
    using namespace std;
    class Circle{
        protected:
            float radius,a;
            public:
                Circle(){
                    cout<<"Enter a value:";
                    cin>>a;
                }
                int area(){
                    a = 3.14*radius*radius;
                }
                int operator ==(Circle c){
                    if(radius >c.radius)
                    return 1;
                    else
                    return 0;
                }
                int display(){
                    cout<<"Area=:"<<a;
                }
    };
    int main(){
        Circle c1;
        Circle c2;
        if(c1==c2)
        cout<<"Equal radius";
        else
        cout<<"Radii are not equal";
    }





