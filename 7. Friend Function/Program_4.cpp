

// Define class TwoDpoint(x,y) and ThreeDpoint(x,y,z).Write a friend function"add" which adds on object of TwoDpoint and ThreeDpoint and return a ThreeDpoint object.


#include<iostream>
#include<stdio.h>
using namespace std;
class ThreeDpoint;
class TwoDpoint{
    int a,b;
    public:
        TwoDpoint(int x,int y){
            a = x;
            b = y;
        }
        friend ThreeDpoint add(TwoDpoint ,ThreeDpoint);
};
class ThreeDpoint{
    int x,y,z;
    public: ThreeDpoint(int a,int b,int c){
            x = a;
            y = b;
            z = c;
        }
        int display(){
            cout<<"ThreeDPoint("<<x<<","<<y<<")";
        }
        friend ThreeDpoint add(TwoDpoint,ThreeDpoint);
};
ThreeDpoint add(TwoDpoint p,ThreeDpoint q){
    ThreeDpoint r(0,0,0);
    r.x = q.x + p.a;
    r.y = q.y + p.b;
    r.z = q.z;
    return r;
}
int main(){
    TwoDpoint a(3,4);
    ThreeDpoint b(3,4,5);
    ThreeDpoint c = add(a,b);
    c.display();
}






