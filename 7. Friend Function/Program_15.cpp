

// Declare a class called book having members like book_title,publisher and author_name. Overload extractor and inserter operator(>>and<<)forclass book.

    #include<iostream>
    #include<stdio.h>
    using namespace std;
    class Book{
        char *title,title_1[20];
        char *publish,publish_1[20];
        char *author,author_1[20];
        public:
            int get_data(){
                title = title_1;
                publish = publish_1;
                author = author_1;
                cout<<"Enter book title,publisher and author name:";
                cin>>title>>publish>>author;
            }
            Book operator>>(Book b3){
                b3.title = title;
                b3.publish = publish;
                b3.author = author;
                return b3;
            }
            int operator <<(Book b3){
                title = b3.title;
                publish = b3.publish;
                author = b3.author;
            }
            int display(){
                cout<<"\nBook title:"<<title<<"\nPublisher:"<<publish<<"\nAuthor:"<<author;
            }
    };
    int main(){
        Book b,b1,b2;
        b.get_data();
        b.display();
        b1=b>>b1;
        b1.display();
        b2<<b1;
        b2.display();
    }





