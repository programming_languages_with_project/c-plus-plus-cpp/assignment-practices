

// Write a program to create a class distance containing feet and inches. Using operator keyword,convert an object of class distance into total meters which is a float data type.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Distance{
        int ft,in;
        float m;
        public:
            int read(){
                cout<<"Enter the distance in feet and inches:";
                cin>>ft>>in;
            }
            int operator ++(){
                m=(ft+in/12.0)/3.28;
            }
            int display(){
                cout<<"Distance in meters="<<m;
            }
    };
    int main(){
        Distance d1;
        d1.read();
        d1++;
        d1.display();
    }



