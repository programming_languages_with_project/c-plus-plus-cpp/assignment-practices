

// WAP to overload the'[]'operator in a class to access data within the class by indexing method.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Aber{
        int a,b,c;
        public:
            A(){
                a = 10;
                b = 20;
                c = 30;
            }
            int operator[](int x){
                if(x==1) return a;
                else if(x==2) return b;
                else if(x==3) return c;
                else return-1;
            }
    };
    int main(){
        Aber a;
        int x;
        cout<<"Enter a number between 1 to 3:";
        cin>>x;
        cout<<a[x];
    }







