

///  Write a program to add two complex number using a friend function.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Complex{
        public:
            int x,y;
            int read(){
                cout<<"Enter the real and imaginary part of the complex number :";
                cin>>x>>y;

            }
        //Friend function for various operation on complex numbers.    
            friend Complex add(Complex c1,Complex c2);
            friend Complex sub(Complex c1,Complex c2);
            friend Complex mul(Complex c1,Complex c2);
            friend Complex div(Complex c1,Complex c2);
            friend Complex conj(Complex c1);

        // Function to deisplay the complex number
            int display(){
                if(y<0)
                cout<<x<<y<<"i";
                else
                cout<<x<<"+i"<<y;

                cout<< endl;      //newline for better readability
            }
    };
        Complex add(Complex c1,Complex c2){
        Complex c3;
        c3.x = c1.x + c2.x;
        c3.y = c1.y + c2.y;
        return c3;

    }
        Complex sub(Complex c1,Complex c2){
        Complex c3;
        c3.x = c1.x - c2.x;
        c3.y = c1.y - c2.y;
        return c3;

    }
        Complex mul(Complex c1,Complex c2){
        Complex c3;
        c3.x = c1.x*c2.x;
        c3.y = c1.y*c2.y;
        return c3;

    }
        Complex div(Complex c1,Complex c2){
        Complex c3;
        c3.x = c1.x/c2.x;
        c3.y = c1.y/c2.y;
        return c3;

    }
        Complex conj(Complex c1,Complex c2){
        Complex c3;
        c3.x = c1.x;
        c3.y = -c2.y;
        return c3;

    }

    int main(){

        Complex c1;
        Complex c2;
        Complex c3;
        
        c1.read();
        c2.read();
                c3=c1.add(c2);
        c3.display();
                c3=c1.sub(c2);
        c3.display();
                c3=c1.mul(c2);
        c3.display();
                c3=c1.div(c2);
        c3.display();
                c3=c1.conj();
        c3.display();
        



    }



