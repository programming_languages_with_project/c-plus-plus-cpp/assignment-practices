

/// Write a program to demonstrate how a common friend function can be used to exchange the private values of two classes.(use call by reference method).


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class B;
    class A{
        protected:
            int a;
                public:
                    A(){
                        a = 5;
                    }
                    int display(){
                        cout<<"The Value of a is "<<a;
                    }
                    friend int exchange(A*a,B*b);
    };
    class B{
        protected:
            int b;
                public:
                    B(){
                        b =10;
                    }
                    int display(){
                        cout<<"\n The value of b is "<<b<<endl;
                    }
                    friend int exchange(A*a,B*b);
    };
                    int exchange(A*p1,B*p2)
                    {
                        int temp;
                        temp = p1->a;
                        p1->a= p2->b;
                        p2->b=temp;
                    }
    
                    int main(){
                        A a1;
                        B b1;
                        exchange(&a1,&b1);
                        a1.display();
                        b1.display();
                    }
    


