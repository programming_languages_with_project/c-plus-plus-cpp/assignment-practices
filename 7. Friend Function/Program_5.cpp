

/// WAP to add a distances entered by the user in feel and inches using overload binary operator."+=".

#include<iostream>
#include<stdio.h>
using namespace std;

class Distance{

    int ft,in;
    public:
        int read(){
            cout<<"Enter the distance in feet and inches :";
            cin>>ft>>in;
        }
        int operator +=(Distance d){
            ft = ft + d.ft;
            in = in + d.in;
            if(in>=12){
                ft++;
                in = in-12;
            }
        }
        int display(){
            cout<<"Distance is "<<ft<<" feet and "<<in<<" inches.";
        }
};
int main(){
    Distance d1;
    Distance d2;
    d1.read();
    d2.read();
    d1+=d2;
    d1.display();
}




