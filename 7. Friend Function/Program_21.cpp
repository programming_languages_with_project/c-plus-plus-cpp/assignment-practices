

/* Design a class Polar which describes a point in the plane using Polar co-ordinates radius and angle.A point in polar Co-ordinates.
            use overloaded + operator to add two object of polar.
            You need to use the following trigonometric formula.
            X = R*cos(A)
            Y = R*sin(A)
            A = atan(Y/N)  //arc tangent
            R = (X*X +Y*Y)
*/

    #include<iostream>
    #include<stdio.h>
    #include<math.h>
    using namespace std;
    class Complex{
        float a,r,x,y;
        public:
            int read(){
                cout<<"Enter the magnitude anf angle of a complex number:";
                cin>>r>>a;
                x=r*cos(a);
                y=r*sin(a);
            }
            Complex operator +(Complex c){
                Complex f;
                f.x = x + c.x;
                f.y = y + c.y;
                return f;
            }
            int display(){
                a = atan(y/x);
                r = sqrt(x*x+y*y);
                cout<<"Sum="<<r<<"/_"<<y;
            }
    };
    int main(){
        Complex c1;
        Complex c2;
        c1.read();
        c2.read();
        Complex c3;
        c3 = c1+c2;
        c3.display();
    }


