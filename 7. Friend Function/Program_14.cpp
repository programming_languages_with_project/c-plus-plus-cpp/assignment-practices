

// Write a C++ class to represent an integer matrix of different diamensions. Overload +,- to perform matrix operation.

    #include<iostream>
    #include<stdio.h>
    #include<vector>
    using namespace std;

    class Matrix{
        int r,c,m[10][10];
        Matrix(int x,int y){
            r = x;
            c = y;
        }
        int read(){
            int i,j;
            cout<<"Enter the element of the matrix:";
            for(int i=0;i<=r-1;i++){
                for(int j=0;j<=c-1;j++){
                    cin>>m[i][j];
                }
            }
        }
        Matrix operator +(Matrix n){
            for(int i=0;i<=r-1;i++){
                for(int j=0;j<=c-1;j++){
                    m[i][j] = m[i][j]+n.m[i][j];
                }
            }
            return this;
        }
        Matrix operator -(Matrix n){
            for(int i=0;i<=r-1;i++){
                for(int j=0;j<=c-1;j++){
                    m[i][j]=m[i][j]-n.m[i][j];
                }
            }
            return this;
        }
        int display(){
            for(int i=0;i<=r-1;i++){
                for(int j=0;j<=c-1;j++){
                    cout<<m[i][j]<<"\t";
                }
                cout<<endl;
            }
        }
    };
    int main(){
        Matrix m(2,2);
        m.accept();
        m.display();
        Matrix n(2,2);
        n.accept();
        n.display();
        m=m+n;
        m.display();
        m=m-n;
        m.display();
    }






