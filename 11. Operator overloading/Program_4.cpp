

/// WAP to overload binary operators "+" to add "-" to subtract "*" to multiply "/" to divide and "-" for conjugate of complex numbers.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Complex{
        public:
            int x,y;
            int read(){
                cout<<"Enter the real and imaginary part of a complex number :";
                cin>>x>>y;
            }
            Complex operator +(Complex c){
                Complex c1;
                c1.x = x + c.x;
                c1.y = y + c.y;
                return c1;
            }
            Complex operator -(Complex c){
                Complex c1;
                c1.x = x - c.x;
                c1.y = y - c.y;
                return c1;
            }
            Complex operator *(Complex c){
                Complex c1;
                c1.x = x*c.x;
                c1.y = y*c.y;
                return c1;
            }
            Complex operator /(Complex c){
                Complex c1;
                c1.x = x/c.x;
                c1.y = y/c.y;
                return c1;
            }
            Complex operator ~(){
                Complex c1;
                c1.x = x ;
                c1.y = y ;
                return c1;
            }
            int display(){
                if(y<0)
                cout<<x<<y<<"i";
                else
                cout<<x<<"+i"<<y;
            }
    };
    int main(){
        
        Complex c1;
        Complex c2;
        Complex c3;
        
        c1.read();
        c2.read();
        
        c3= c1+c2;
        c3.display();

        c3=c1-c2;
        c3.display();
        
        c3= c1*c2;
        c3.display();
        
        c3= c1/c2;
        c3.display();
        
        c3= ~c1;
        c3.display();
    }






















