

/// Define class integer having one int data member. Overload the following operators: ++(pre and post),(unary and binary).


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Negate{
        public: 
            int x,y;
            int read(){
                cout<<"Enter two number :";
                cin>>x>>y;
            }
            int operator -(){
                x=x;
                y=y;
            }
            int operator -(Negate p){
                x=x-p.x;
                y=y-p.y;
            }
            int operator ++(){
                x++;
                y++;
            }
            int display(){
                cout<<"x="<<x<<endl<<"y="<<y;
            }
    };
    int main(){
        Negate n1,n2;
        n1.read();
        n2.read();
        
        -n1;
        cout<<"After unary - on first object\n";
        n1.display();
        n1-n2;
        cout<<"After binary -on first and second object\n";
        n1.display();
        
        n2++;
        cout<<"After post increment and on second object\n";
        n2.display();
        ++n2;
        cout<<"After pre increment on second object\n";
        n2.display();

        return 0;
    }



