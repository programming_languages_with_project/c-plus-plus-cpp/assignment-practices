

/// WAP to add two distances entered by the user in feet and inches using overload binary operator "+".


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Distance{
        public:
            int ft,in;
            int read(){
                cout<<"Enter the distance in feet and inches:";
                cin>>ft>>in;
            }
            Distance operator +(Distance d){
                Distance dr;
                dr.ft = ft+d.ft;
                dr.in = in+d.in;
                if(dr.in>=12){
                    dr.ft++;
                    dr.in=dr.in-12;
                }
                return dr;
            }
            int display(){
                cout<<"Distance is "<<ft<<" feet and "<<in<<" inches.";
            }
    };
    int main(){
        Distance d1;
        Distance d2;
        Distance d3;
        d1.read();
        d2.read();
        d3 = d1+d2;
        d3.display();
    }


