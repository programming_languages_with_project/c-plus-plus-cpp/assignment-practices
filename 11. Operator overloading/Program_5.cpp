

/// WAP to program to overload binary operator "+=" to add two complex numbers.

    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Complex{
        public:
            int x,y;
            int read(){
                cout<<"Enter the real and imaginary parts of a complex number :";
                cin>>x>>y;
            }
            int operator+=(Complex c){
                x=x+c.x;
                y=y+c.y;
            }
            int display(){
                if(y<0)
                cout<<x<<y<<"i";
                else
                cout<<x<<"+i"<<y;
            }
    };
    int main(){
        Complex c1;
        Complex c2;
        c1.read();
        c2.read();
        c1+=c2;
        c1.display();
    }



