

/// WAP to negate the values of two variable contained in an Object by overloading the operator unary negate i.e. '-' .

    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Negate{
        public:
        int x,y;
        int read(){
            cout<<"Enter two number :";
            cin>>x>>y;
        }
        int operator-(){
            x=-x;
            y=-y;
        }
        int display(){
            cout<<"x="<<x<<endl<<"y="<<y;
        }
    };
    int main(){
        
        Negate n;
        n.read();
        -n;
        n.display();
    }



