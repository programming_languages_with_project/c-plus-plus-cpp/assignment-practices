

// WAP to overload unary operators i.e. increment and decrement.

    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class IncDec{
        public:
            int x,y;
            int read(){
                cout<<"Enter two number :";
                cin>>x>>y;
            }
            int operator--(){
                x--;
                y--;
            }
            int operator++(){
                x++;
                y++;
            }
            int display(){
                cout<<"x="<<x<<endl<<"y="<<y<endl;
            }
    };
    int main(){
        IncDec n;
        n.read();
        --n;
        cout<<"After decrementing the object one time\n";
        n.display();
        ++n;
        ++n;
        cout<<"After incrementing the object twice\n";
        n.display();
    }

