

/// Write a program to calculate the value of the following series using internal member function.
///         S = 1+2+3+4+.......+n



    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class series{

        int n,sum;
        public:
            int read(){
                cout<<"Enter the value of n :";
                cin>>n;
            }
            int compute(){
                for(int i=1;i<=n;i++){
                    sum = sum+i*i;
                }
            }
            int display(){
                cout<<"Value of the series ="<<sum;
            }
    };
    int main(){

        series s;
        s.read();
        s.compute();
        s.display();
    }

