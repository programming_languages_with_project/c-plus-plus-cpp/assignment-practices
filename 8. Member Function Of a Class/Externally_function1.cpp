

/// Write a program to find area of a circle using Object Oriented Programming such that the class circle must have three externally define member function namely.

    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class circle{
        float r,a;
        public:
            void read();
            void compute();
            void display();
    };
    void circle::read(){
        cout<<"Enter a Radius :";
        cin>>r;
    }
    void circle::compute(){
        a = 3.14*r*r;
    }
    void circle::display(){
        cout<<"Area = "<<a;
    }
    
    int main(){

        circle c;
        c.read();
        c.compute();
        c.display();
    }
 




