

/// WAP to calculate the value of the following series using external member function.
//    S = 1^2 + 2^2 + 3^2+ 4^2+ ......... + n^2


    #include<iostream>
    #include<stdio.h>
   // using namespace std;

    class series{
        int n,sum=0;
        public:
            int read();
            int compute();
            int display();
    };
    int series::read(){
        std::cout<<"Enter the value of n: ";
        std::cin>>n;
    }
    int series::compute(){
        for(int i=1;i<=n;i++){
            sum = sum+i*i;
        }
    }
    int series::display(){
        std::cout<<"Value of the series ="<<sum;
    }
    int main(){
        series s;
        s.read();
        s.compute();
        s.display();
    }






