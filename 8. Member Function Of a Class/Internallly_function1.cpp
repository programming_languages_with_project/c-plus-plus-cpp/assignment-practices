

/// Write a program to find area of a circle using Object Oriented Programming such that the class circle must have three member function namely:



    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class circle{

        float r,a;
        public:
            int read(){
                cout<<"Enter radius :";
                cin>>r;
            }
            int compute(){
                a = 3.14*r*r;
            }
            int display(){
                cout<<"Area = "<<a;
            }
    };
    int main(){

        circle c;
        c.read();
        c.compute();
        c.display();
    }



