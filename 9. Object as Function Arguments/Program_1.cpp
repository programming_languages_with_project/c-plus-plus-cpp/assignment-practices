

/// Write a program to add two complex numbers using objects of class Complex.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Complex{
        public:
            int x,y;
            int read(){
                cout<<"Enter the Real and Imaginary parts of a complex number:";
                cin>>x>>y;
            }
            Complex add(Complex c){
                Complex c1;
                c1.x = x + c.x;
                c1.y = y + c.y;
                return c1;
            }
            void display(){
                if(y<0)
                cout<<x<<y<<"i";
                else
                cout<<x<<"+i"<<y;
            }
    };
    int main(){

        Complex c1;
        Complex c2;
        Complex c3;
        c1.read();
        c2.read();
        c3 = c1.add(c2);
        c3.display();
    }



