// Write a program to create a vector class template to add,delete and display values form the vector.

#include<iostream>
using namespace std;

template<class vector> int insert(vector data,vector array[],int size){
    array[size] = data;
    return(size+1);
}
template<class vector>int remove(vector data,vector array[],int size){
    int i;
    for(int i=0;i<=size-1;i++){
        if(data==array[i])
        break;
    }
    while(i<=size-2){
        array[i]=array[i+1];
        i++;
    }
    return(size-1);
}
template<class vector>int display(vector array[],int size){
    int i;
    for(int i=0;i<=size-1;i++){
        cout<<array[i]<<"\t";
    }
    cout<<endl;
}
int main(){
    int m,n,i,a[100];
    float b[100],x;
    cout<<"Enter size of Integer array :";
    cin>>n;
    for(int i=0;i<=n-1;i++){
        cout<<"Enter an integer:";
        cin>>a[i];
    }
    cout<<"1.Add an element\n2.Delete an element\n3.Display\nEnter your choice:";
    cin>>i;
    switch(i){
        case 1:
            cout<<"Enter element to insert :";
            cin>>i;
            n=insert(i,a,n);
            cout<<"After insertion:\n";
            display(a,n);
            break;
        case 2:
            cout<<"Enter element to be deleted :";
            cin>>i;
            n=remove(i,a,n);
            cout<<"After deletion :\n";
            display(a,n);
            break;
        case 3:
            display(a,n);
            break;  
        default:
            cout<<"Invalid choice \n";          
    }
    cout<<"Enter size of float array:";
    cin>>m;
    for(int i=0;i<=m;i++){
        cout<<"Enter a float number:";
        cin>>b[i];
    }
    cout<<"1.Add an element\n2.Delete an element\n3.Display\nEnter your choice:";
    cin>>i;
    switch(i){
        case 1:
            cout<<"Enter element to insert :";
            cin>>x;
            m = insert(x,b,m);
            cout<<"After insertion :\n";
            display(b,m);
            break;
        case 2:
            cout<<"Enter element to be deleted :";
            cin>>x;
            m = remove(x,b,m);
            cout<<"After deletion :\n";
            display(b,m);
            break;
        case 3: 
            display(b,m);
            break;
        default:
        cout<<"Invalid choice \n";
    }
}








