//write s program to demonstrate the use of try catch block with the arguments as an integer and a string using multiple catch blocks.


#include<iostream>
using namespace std;

int test(int x){
    try{
        if(x>=0 && x<=9)throw x;
        else throw"Not a single digit number";
    }
    catch(int i){
        cout<<"The number is single digit";
    }
    catch(char a[100]){
        cout<<a;
    }
}
int main(){
    int p;
    cout<<"Enter a number :";
    cin>>p;
    test(p);
}








