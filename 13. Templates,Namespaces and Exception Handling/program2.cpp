// Write a program to write a generic function or template and demonstrate swapping of multiple types of data using the same.

#include<iostream>
using namespace std;

template<class x> int swapValues(x &a,x &b){
    x temp;
    temp =a;
    a = b;
    b = temp; 
}
int main(){
    int a =4,b=5;
    float p=3.5,q=4.2;
    cout<<"a="<<a<<"\tb="<<b<<"before calling swap function"<<endl;
    swapValues(a,b);
    cout<<"a="<<a<<"\tb="<<b<<"after calling swap function"<<endl;
    cout<<"p="<<p<<"\tq="<<q<<"before calling swap function"<<endl;
    swapValues(p,q);
    cout<<"p="<<p<<"\tq="<<q<<"after calling swap function"<<endl;

return 0;
}








