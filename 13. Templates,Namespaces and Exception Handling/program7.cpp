// write a c++ program using a class template to read any parameterized data type such as float and integer and print them in sorted form.

#include<iostream>
using namespace std;

template<class x>int sort(x a[],int n){
    int i,j;
    x temp;
    for(int i=0;i<=n-2;i++){
        for(int j=0;j<=n-2;j++){
            if(a[j]>a[j+1]){
                temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }
}
int main(){
    int a[100],m,n,i;
    float b[100];
    cout<<"Enter number of integer :";
    cin>>n;
    for(int i=0;i<n-1;i++){
        cout<<"Enter an integer :";
        cin>>a[i];
    }
    sort(a,n);
    cout<<"Sorted array :\n";
    for(int i=0;i<=n-1;i++){
        cout<<a[i]<<"\t";
    }
    cout<<endl;
    cout<<"Enter number of float numbers :";
    cin>>m;
    for(int i=0;i<=m;i++){
        cout<<"Enter a float number:";
        cin>>b[i];
    }
    sort(b,m);
    cout<<"Sorted array:\n";
    for(int i=0;i<=m-1;i++){
        cout<<b[i]<<"\t";
    }
    cout<<endl;
}





