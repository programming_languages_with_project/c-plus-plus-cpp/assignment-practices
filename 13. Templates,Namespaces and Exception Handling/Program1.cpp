// Write a program to write a generic function or template and demonstrate addition of multiple types of data using the same.

#include<iostream>
using namespace std;

template<class x>x add(x a,x b){
    return(a+b);
}
int main(){
    cout<<add(3,5)<<endl;
    cout<<add(3.5,6.2)<<endl;
    cout<<add(2.0,4.5)<<endl;
}








