// Declare a template class called exam having an array of generic type as a data member,named elements[10],define following generic(template)member functions:

#include<iostream>
using namespace std;

template<class exam>int sort(exam a[],int n){
    int i,j;
    exam temp;
    for(int i=0;i<=n-2;i++){
        for(int j=0;j<=n-2;j++){
            if(a[j]>a[j+1]){
                a[j+1] =temp;
                a[j]=a[j+1];
                a[j+1] = temp;
            }    
        }
    }
}
template<class exam>exam max(exam a[],int n){
    exam large;
    int i;
    large = a[0];
    for(int i=1;i<=n-1;i++){
        if(a[i]>large)
        large =a[i];
    }
    return large;
}
int main(){
    int a[100],n,i;
    float b[100];
    cout<<"Enter number of integers:";
    cin>>n;
    for(int i=0;i<=n-1;i++){
        cout<<"Enter an integer:";
        cin>>a[i];
    }
    sort(a,n);
    cout<<"Sorted array :\n";
    for(int i=0;i<=n-1;i++){
        cout<<a[i]<<endl;
    }
    cout<<"Largest number:"<<max(a,n)<<endl;
    cout<<"Enter number of float numbers:";
    cin>>n;
    for(int i=0;i<=n-1;i++){
        cout<<"Enter a float number :";
        cin>>b[i];
    }
    sort(b,n);
    cout<<"Sorted array:\n";
    for(int i=0;i<=n-1;i++){
        cout<<b[i]<<endl;
    }
    cout<<"Largest number:"<<max(b,n)<<endl;
}








