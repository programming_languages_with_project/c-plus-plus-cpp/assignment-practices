//write a C++ program using a class template to read any five parameterized data type such as float and integer and print the average.


#include<iostream>
using namespace std;

template<class x>float avg(x a,x b,x c,x d,x e){
    float average;
    average = (a+b+c+d+e)/5.0;
    return average;
}
int main(){
    int p,q,r,s,t;
    float v,w,x,y,z;
    
    cout<<"Enter five integers :";
    cin>>p>>q>>r>>s>>t;
    cout<<"Average ="<<avg(p,q,r,s,t)<<endl;
    
    cout<<"Enter five float numbers.";
    cin>>v>>w>>x>>y>>z;
    cout<<"Average ="<< avg(v,w,x,y,z)<<endl;
}



