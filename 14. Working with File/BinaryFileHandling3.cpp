// Write s program to display the contents of a text file in the reverse order(user pointer manipulation).


#include<iostream>
#include<fstream>
#include<conio.h>
using namespace std;

int main(){

    char text[200],c;
    int i=0;
    ifstream in;
    in.open("test0",ios::out);
    while(!in.eof()){
        in.get(c);
        text[i]=c;
        i++;
    }
    i--;
    while(i>=0){
        cout<<text[i];
        i--;
    }
    in.close();
}

