

//  WAP to program to evalute the value of the 'standard deviation (s.d)' and display the result.
                    
#include<iostream>
#include<math.h>
using namespace std;

int main(){
    
    int n,a[100],sum=0;
    float avg,sum1=0,sd;
    
    cout<<"Enter the number of elements : ";
    cin>>n;

    for(int i=0;i<=n-1;i++){
    
        cout<<"Enter the value : ";
        cin>>a[i];
        sum = sum + a[i];
    }
    avg = sum;
    avg = avg/n;   // calculate the average 

// Calculate the sum of the squared difference from the mean
    for(int i=0;i<=n-1;i++){
    
        sum1 = sum1 + pow(a[i] - avg,2);
    }
    sd = sum1/n;
    sd = pow(sd,0.5);
    cout<<" The standard deviation is "<<sd;
}

