

/*  WAP to evalute the value of the following series and display the result.
                    n        n
                    ∑  x^2 i - [ ∑ x i ]^2
                   i=1      i=1    
*/

#include<iostream>
using namespace std;

int main(){

    int n,i,a[100],sum1=0,sum2=0,sum;
    cout<<"Enter the number of elements : ";
    cin>>n;

    for(int i=0;i<=n-1;i++){
        cout<<"Enter the value :\t";
        cin>>a[i];
    }

        // In second for loop series value added in declared sum variable and add and multiply value. 
    for(int i=0;i<=n-1;i++){
        sum1= sum1 + a[i]*a[i];
        sum2 = sum2+a[i];
    }
    sum = sum1 - sum2*sum2;
    cout<<"The value of the series is " << sum;
}

