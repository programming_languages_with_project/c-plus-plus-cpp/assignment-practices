

// WAP to find and display the reserve of an array using a function.

#include<iostream>
using namespace std;

int main(){

    int n,a[100];
// Prompt the user to enter the number of elements
    cout<<"Enter the value of elements : \n";
    cin>>n;
    for(int i=0;i<=n-1;i++){
        cout<<"Enter the value ";
        cin>>a[i];
    }
    void reverse(int a[],int n);
    reverse(a,n);    // Call the function to reserve the array
}


// Function define to reserves the array
void reverse(int a[],int n){
    int i,temp;


  //Loop ot swap the elements from the ends towards the center.  
    for(int i=0;i<=(n-1)/2;i++){
        temp = a[n-i-1];
        a[n-i-1] = a[i];
        a[i] = temp;
    }
    cout<<" The reverse of this array is : \n";
    for(int i=0;i<=n-1;i++){
        cout<<a[i]<<endl;
    }
}

