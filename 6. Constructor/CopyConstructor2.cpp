

/// WAP to calculate the value of the following series using copy constructor and inline member function.
//    S = 1^2 + 2^2 + 3^2+ 4^2+ ......... + n^2



    #include<iostream>
    #include<stdio.h>
    using namespace std;

// class to compute sum of square of first n natural numbers
    class series{

        int i,n,sum;
        public:
        // Constructor to initialize n and sum
            series(int x){
                n=x;
                sum=0;
            }
            //Copy constructor
            series(series &x){
                n=x.n;
                sum=0;
            }
            int compute();
            int display();
    };
    
    inline int series::compute(){
        for(int i=1;i<=n;i++){
            sum = sum+i*i;
        }
    }
    inline int series::display(){
        cout<<"Value of the series ="<<sum;
    }
    int main(){
        int x;
        cout<<"Enter the value of n:";
        cin>>x;
        
        series s(x);
        s.compute();
        s.display();
        
        series s2(x); // demonstrting the copy constructor
        s2.compute();
        s2.display();
    }

