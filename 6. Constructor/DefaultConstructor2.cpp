



/// WAP to calculate the value of the following series using external member function.
//    S = 1^2 + 2^2 + 3^2+ 4^2+ ......... + n^2



    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class series{

        public:
            int i,n,sum;
            series(){           // Constructor to initialize n and sum,and read n from user input
                cout<<"Enter the value of n:";
                cin>>n;
                sum=0;
            }
            int compute();
            int display();
    };

    inline int series::compute(){
        int sum=0;
        for(int i=1;i<=n;i++){
            sum = sum+i*i;
        }
    }
    inline int series::display(){
        cout<<"Value of the series ="<<sum;
    }
    int main(){
        series s;
    
        s.compute();
        s.display();
    }








