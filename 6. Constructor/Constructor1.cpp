

    // COnstructor with Default Arguments.


    
    /// Illustrate the concept of constructor with default argument with suitable example.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Interest{

        private:
            static float rate;
            float principal,duration,sim_int;
            public:
                Interest(float x =1000,float y =10){
                    principal = x;
                    duration = y;
                }
                void calculate(){
                    sim_int = principal*rate*duration/100;
                }
                void display(){
                    cout<<"Simple Interest="<<sim_int;
                }
    };
    float Interest::rate=10;        // Initialize static variable rate

    int main(){

        float x,y;
        cout<<"Enter principle amount :";
        cin>>x;
        Interest i(x);
        i.calculate();
        i.display();
    }



