

///  Write a program to find area of a circle using Object Oriented Programming .
///  The value of the radius must be accepted from the user in the constructor and the class circle must have two inline function namely.


    #include<iostream>
    
    using namespace std;

    class circle{

        float r,a;
        public:
            circle(){           // Costructor to read the radius from user input
                cout<<"Enter the value of radius :";
                cin>>r;
            }
            int compute();
            int display();
    };
    inline int circle::compute(){
        a = 3.14*r*r;
    }
    inline int circle::display(){
        cout<<"Area ="<<a;
    }
    int main(){

        circle c;
        c.compute();
        c.display();
    }









