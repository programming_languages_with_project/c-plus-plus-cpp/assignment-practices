



///  Write a program to find area of a circle using Object Oriented Programming .
///  The value of the radius must be accepted from the user in the main program and passed to the parameterized constructor and the class circle must have two inline function namely.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class circle{

        float r,a;
        public:
            circle(float x){    // Constructor to initialize radius
                r = x;
            }
            int compute();
            int display();
    };
    inline int circle::compute(){   //inline function to compute the area of the circle
        a = 3.14*r*r;
    }
    inline int circle::display(){
        cout<<"Area ="<<a;
    }
    int main(){
        float p;
        cout<<"Enter the radius of the circle";
        cin>>p;
        circle c(p);
        c.compute();
        c.display();
    }









