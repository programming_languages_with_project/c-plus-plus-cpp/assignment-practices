

///  Write a program to demonstrate the destructor.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class test{
       
        public:
            int*p;
            test(){             // Constructor to allocate memory
                p=new int;
            }
            void read(){
                cout<<"Enter a number";
                cin>>p;
            }
            void display(){
                cout<<"Value ="<<*p<<endl;
            }
            ~test(){                //Destructor to deallocate memory and display a message
                delete p;
                cout<<"Destroyed ";
            }
    };
    int main(){

        test t;
        t.read();
        t.display();

    // the destructor is called automatically when the object goes out of scope
        return 0;
    }








