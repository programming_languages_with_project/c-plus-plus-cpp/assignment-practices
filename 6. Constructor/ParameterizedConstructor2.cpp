

/// WAP to calculate the value of the following series using external member function.
//    S = 1^2 + 2^2 + 3^2+ 4^2+ ......... + n^2



    #include<iostream>
    #include<stdio.h>
   // using namespace std;

    class series{

        int i,n,sum;
        public:
        //constructor to initialize n and sum
            series(int x){
                n=x;
                sum=0;
            }
            int compute();  // Function to compute the sum of squares series
            int display();
    };

    inline int series::compute(){       // inline function to compute the sum of square series
        for(int i=1;i<=n;i++){
            sum = sum+i*i;
        }
    }
    inline int series::display(){
        cout<<"Value of the series ="<<sum;
    }
    int main(){
        int x;
        cout<<"Enter the value of n :";
        cin>>x;
        series s(x);
        s.compute();
        s.display();

        return 0;
    }








