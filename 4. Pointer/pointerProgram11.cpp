

///     Write a program to find reverse of a string using pointer to string.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    int main(){

        char *str,temp;
        int i,n;
        str = new char[20];
        cout<<"Enter a string :";
        gets(str);
        n=0;
        while(str[n]!=0){
            n++;
        }
        for(int i=0;i<=(n-1)/2;i++){
            temp = *(str+i);
            *(str+i)=*(str+n-i-1);
            *(str+n-i-1)=temp;
        }
        cout<<"Reverse String : "<<str;
    }






