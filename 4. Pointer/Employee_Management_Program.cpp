

#include<iostream>

using namespace std;

struct emp
{
   int empId;
   string empName;
   float empBasic;
   float empExperience;
};

void display(emp *e);
void increase(emp *e);

int main()
{
   emp mgr, *eptr;

   cout<<"Enter the employee ID: ";
   cin>>mgr.empId;
   cout<<"Enter the name: ";
   cin>>mgr.empName;
   cout<<"Enter basic pay: ";
   cin>>mgr.empBasic;
   cout<<"Enter the experience (in years): ";
   cin>>mgr.empExperience;

   eptr = &mgr;

   cout<<"\nEmployee details before increase()\n";
   display(eptr);

   increase(eptr);
   
   cout<<"\nEmployee details after increase()\n";
   display(eptr);

   cout<<endl;
   return 0;
}

void display(emp *e)
{
   int len = (e->empName).size();

   cout<<"Employee ID: "<<e->empId;
   cout<<"\nName: "<<e->empName;
   cout<<"\tBasic: "<<e->empBasic;
   cout<<"\tExperience: "<<e->empExperience<<" years\n";
}

void increase(emp *e)
{
   if(e->empExperience >= 5)
   {
      e->empBasic = e->empBasic + 15000;
   }
}