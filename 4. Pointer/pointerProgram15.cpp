

///     Write a C++ program witn ComputeSphere() function that return the volume V and the Surface area S of a sphere with given radius r.
///  void ComputerSphere (float &s,float &v,float r)


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    int ComputeSphere(float &s,float &v,float r){

        s = 4*3.14*r*r;
        v = 4*3.14*r*r*r/3;
    }
    int main(){

        float s,v,r;
        cout<<"Enter radius of sphere :";
        cin>>r;
        ComputeSphere(s,v,r);
        cout<<"Surface Area ="<<s<<"\nVolume ="<<v;
    }

