

///     Call By Reference =['']
/// WAP  a program to swap two number using a function. Pass the values to be swapped to this function using call by reference method.



    #include<iostream>
    #include<stdio.h>
    using namespace std;

    int main(){

        int a,b;
        cout<<"Enter the number :";
        cin>>a>>b;
        int swap(int *p1,int *p2);
        cout<<"The value of a and b in the main function n before calling the swap function are "<<a<<" and "<<b<<endl;
        swap(&a,&b);
        cout<<"The value of a and b in the main function after calling the swap function are "<<a<<" and "<<b<<endl;

    }
    int swap(int *p1,int *p2){
        int temp;
        temp=*p1;
        *p1=*p2;
        *p2=temp;
        cout<<"The value of a and b in the swap function after swapping are "<<*p1<<" and "<<*p2<<endl;
    }












