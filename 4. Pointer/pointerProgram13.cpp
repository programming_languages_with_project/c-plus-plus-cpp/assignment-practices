

///  WAP to accept a set of 10 number and print the numbers using pointers.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    int main(){

        int *p,i,n;
        p=new int[10];
        cout<<"Enter 10s nos."<<endl;
        for(int i=0;i<=9;i++)
        cin>>*(p+i);
        for(int i=0;i<=9;i++)
        cout<<*(p+i)<<endl;

    }

///  "new" operator allocated memory to a pointer.
///        Pointer_name = new Data_type;