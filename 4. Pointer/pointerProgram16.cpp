

///  Write a C++ program to swap two integer numbers.Write separate swap function using reference variable.




    #include<iostream>
    #include<stdio.h>
    using namespace std;

    int main(){

        int a,b;
        cout<<"Enter the numbers :";
        cin>>a>>b;
        int swap(int &a,int &b);
        cout<<"The value of a and b in the main function n before calling the swap function are "<<a<<" and "<<b<<endl;
        swap(a,b);
        cout<<"The value of a and b in main function after calling the swap function are "<<a<<" and "<<b<<endl;

    }
    int swap(int &a,int &b){
        int temp,*p1,*p2;
        temp = a;
        a = b;
        b = temp;
        cout<<"The value of a and b in the swap function after swapping are"<<*p1<<" and "<<*p2<<endl;
    }

