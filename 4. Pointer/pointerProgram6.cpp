

/// Call By Value 

/// Write a program to swap two number using a function.pass the values to be swapped to this function using call by value method.


    #include<iostream>
    #include<stdio.h>

    using namespace std;
    int main(){

        int a,b;
        cout<<"Enter the Two Number :";
        cin>>a>>b;
        int swap(int a,int b);
        cout<<"The value of a and b in the main function before calling the function are "<<a<<" and "<<b<<endl;
        swap(a,b);
        cout<<"The value of a and b in the main function after calling the swap function are "<<a<<" and "<<b<<endl;
    }

    int swap(int a,int b){
        int temp;
        temp = a;
        a = b;
        b =temp;
        cout<<"The main value of a and b in the swap function after swapping are "<<a<<" and "<<b<<endl;
    }





