
/*  Write a program to add two matrices of size m×n.
*/


    #include<iostream>
    using namespace std;

    int main(){
        int m,n,a[10][10],b[10][10],c[10][10];
        cout<<"Enter the number of row and columns : ";
        cin>>m>>n;

    // Read the elements of the first matrix from the user
        cout<<"Enter the elements if Matrix 1 :\n";
        for(int i=0;i<=m-1;i++){
            for(int j=0;j<=n-1;j++){
                cout<<"Enter the Value :";
                cin>>a[i][j];            // Store the input value int the first matrix
            }
        }

    // Read the elements of the first matrix from the user
        cout<<"Enter the element of Matrix 2 :\n";
        for(int i=0;i<=m-1;i++){
            for(int j=0;j<=n-1;j++){
                cout<<"Enter a Value :";
                cin>>b[i][j];           //Store the input value int the second matrix.
            }
        }
        for(int i=0;i<=m-1;i++){
            for(int j=0;j<=n-1;j++){
                c[i][j] = a[i][j] + b[i][j];
            }
        }

    //Display the sum of the two matrices
        cout<<"The sum of two mtrices is: \n";
        for(int i=0;i<=m-1;i++){
            for(int j=0;j<=n-1;j++){
                cout<<c[i][j]<<"\t";
            }
            cout<<endl;     // More to the next line after priting each row.
        }

    }

