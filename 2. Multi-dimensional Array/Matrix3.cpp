

/*      
        WAP tp find and display the largest of all the elements of m×n matrix.
*/


    #include<iostream>
    using namespace std;

    int main(){
    int n,m,a[10][10],large;
    cout<<"Enter the number of rows and column :";
    cin>>m>>n;
    
    // Read the elements of the matrix from the user in loop condition. 
    for(int i=0;i<=m-1;i++){
        for(int j=0;j<=n-1;j++){
            cout<<" Enter a value : ";
            cin>>a[i][j];  
        }
    }
    
    large = a[0][0];     // Initialize 'large' with the first element of the matrix

    // simplified Loop condition find the largest element in the matrix
    for(int i=0;i<=m-1;i++){        
        for(int j=0;j<=n-1;j++){
            if(large<a[i][j]){
                large = a[i][j];
            }
        }
        cout<<"The largest element in the matrix is "<<large<<endl;
    }
    }
