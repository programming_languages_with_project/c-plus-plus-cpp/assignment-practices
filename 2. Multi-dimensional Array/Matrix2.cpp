

/*      WAP to calculate and display the average od all the elements of a m×n matrix.
*/      

#include<iostream>
using namespace std;

int main(){
    int m,n,a[10][10],sum=0,avg;
    
    cout<<"Enter the number of rows and columns :";
    cin>>m>>n;
    
    //
    for(int i=0;i<=m-1;i++){
        
        for(int j=0;j<=n-1;j++){
        
            cout<<"Enter the Value :";
            cin>>a[i][j];
        }
    }

   // Calculate the sum of all matrix elements 
    for(int i=0;i<=m-1;i++){
    
        for(int j=0;j<=n-1;j++){
    
            sum =sum + a[i][j];
        }
    }
    // 2 average of the matrix elements.
    avg = sum/(m*n);
    cout<<"The average is equal to "<<avg;

}


