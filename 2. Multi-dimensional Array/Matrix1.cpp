

/*  WAP to accept an M×N matrix and display it in natural form.
*/

#include<iostream>
using namespace std;

int main(){
    int m,n,a[10][10];     // A 2D array to store the matrix elements.
    cout<<"Enter the number of row and column :";
    cin>>m>>n;

   //Reading matrix elements and simplify Loop conditions. 
    for(int i=0;i<=m-1;i++){
        for(int j=0;j<=n-1;j++){
            cout<<"Enter the value : ";
            cin>>a[i][j];
        }
    }
   // Display the entered matrix 
    cout<<"The entered matrix is :\n ";
    for(int i =0;i<=m-1;i++){
    
        for(int j=0;j<n-1;j++){
            cout<<a[i][j]<<"\t";
        }
        cout<<endl;
    }
}

