// Define ba base class"Item"(item_no,name,price). Drive a class"Discounted_item(discount_percent).A customer buys 'n' items.
// Calculate the total price,total discount and display the bill using appropriate output formats.


#include<iostream>
using namespace std;

class Item{
    protected:  
        char name[20];
        int no,price;
};
class Discounted_Item:public Item{
    int discount_percent,discounted_price;
    public: 
        int accept(){
            cout<<"Enter name,no,price and discount_percent :";
            gets(name);
            cin>>no>>price>>discount_percent;
            discounted_price = price - price * discount_percent / 100;
        }
        int display(){
            cout<<name<<"\t"<<no<<"\t"<<price<<"\t"<<discount_percent<<"\t"<<discounted_price<<"\n";
        }
        int get_price(){
            return price;
        }
        int get_disc(){
            return(price - discounted_price);
        }
};
int main(){
    int i,n,disc=0,price=0;
    Discounted_Item m[100];
    cout<<"Enter the number of items :";
    cin>>n;
    for(int i=0;i<n-1;i++){
        m[i].accept();
    }
    cout<<"Name\tNo\tPrice\tDisc%\tDisc.Price\n";
    for(int i=0;i<=n-1;i++){
        m[i].display();
    }
}







