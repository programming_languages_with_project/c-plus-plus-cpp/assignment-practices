// Write a program to implement inheritances.


#include<iostream>
using namespace std;

class Staff{
    protected:
        char name[20];
        int code;
};
class Teacher:public Staff{
    private:
        char subject[20];
        int experience;
        public:
            int read(){
                cout<<"Enter the Code,Subject and Experiences of the Teacher:"<<endl;
                gets(name);
                cin>>code;
                gets(subject);
                cin>>experience;
            }
            int display(){
                cout<<"Teacher Details:\nName:"<<name<<"\nCode:"<<code<<"\nSubject:"<<subject<<"\nExperience:"<<experience;
            }
};
class Office:public Staff{
    private:
        char dept[20];
        int grade;
        public:
            int read(){
                cout<<"Enter Name,Code,Department and Grade of the Office :"<<endl;
                gets(name);
                cin>>code;
                gets(dept);
                cin>>grade;
            }
            int display(){
                cout<<"Office Details\nName:"<<name<<"\nCode:"<<code<<"\nDepartment:"<<dept<<"\nGrade:"<<grade;
            }
};
int main(){
    int choice;
    cout<<"1.Teacher\n2.Office\nEnter the choice, Whose detail you want to enter:";
    cin>>choice;
    switch(choice){
        case 1:
            Teacher t;
            t.read();
            t.display();
            break;
        case 2:
            Office o;
            o.read();
            o.display();
            break;
        default:
            cout<<"Invalid choice";
    }
}





