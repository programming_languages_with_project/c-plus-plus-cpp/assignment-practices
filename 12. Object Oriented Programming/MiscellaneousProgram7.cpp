// A sale item class inherits from two separate classes: Item(id,description,price) and DiscountOffer(discount Percent).
// A customer purchases n SaleItems.Write a program to display the item-wise bill and total amount using appropriate format.


#include<iostream>
using namespace std;

class Item{
    protected:  
        char name[20];
        int no,price;
};
class Discount_Offer{
    protected:  
        int discount_percent;
};
class Sale_Item : public Item,public Discount_Offer{
    int discounted_price;
    public:
        int accept(){
            cout<<"Enter name, no,price and discount_percent :";
            //gets(name);
            cin>>name;
            cin>>no>>price>>discount_percent;
            discounted_price = price - price * discount_percent /100;
        }
        int display(){
            cout<<name<<"\t\t"<<no<<"\t"<<price<<"\t"<<discount_percent<<"\t"<<discounted_price<<"\n";
        }
        int get_price(){
            return price;
        }
        int get_disc(){
            return(price - discounted_price);
        }
};
int main(){
    int i,n,disc=0,price=0;
    Sale_Item m[100];
    cout<<"Enter the number of items :";
    cin>>n;
    for(int i=0;i<=n-1;i++){
        m[i].accept();
    }
    cout<<"Name\t\tNo\tPrice\tDisc%\tDisc.Price\n";
    for(int i=0;i<=n-1;i++){
        m[i].display();
    }
    for(int i=0;i<=n-1;i++){
        price+=m[i].get_price();
    }
    for(int i=0;i<=n-1;i++){
        disc+=m[i].get_disc();
    }
    cout<<"Total Price    ="<<price<<"\nTotal Discount ="<<disc<<endl;
    cout<<endl;
}
