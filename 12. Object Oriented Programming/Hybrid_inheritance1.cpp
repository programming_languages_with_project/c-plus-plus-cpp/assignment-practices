// Write a program to define the following realation using hybrid inheritance.

#include<iostream>

class Student{
    protected:
        char name[20];
        int roll_no;
};
class Test:public Student{
    protected:  
        int marks;
            public: 
                int read(){
                    std::cout<<"Enter Name,roll number and marks obatained:"<<std::endl;
                    gets(name);
                    std::cin>>roll_no>>marks;
                }
};
class Sports{
    protected:  
        int score;
        public: 
            int accept(){
                std::cout<<"1. Student has won in national sport event\n2. Student has not won in any national sport event\n Enter your choice:";
                std::cin>>score;
            }
};
class Result:public Test,public Sports{
    int total;
    public:
        int calculate(){
            if(score==1)
            total= marks+15;
            else
            total=marks;
        }
        int display(){
            std::cout<<"The total is "<<total;
        }
};
int main(){
   
    Result r;
    r.read();
    r.accept();
    r.calculate();
    r.display();
}





