// The class Result derives information form the classes Internal,University and External respectively.
// The Internal and External respectively. The Internal and External classes access information from the Student class, 
// Define all five classes and write a suitable program to create and display the information contained in Result object.


#include<iostream>

class Student{
    protected:
        char name[20];
        protected:
            int en_no;
};
class Internal:public Student{
    protected:  
        int internal_marks;
};
class External:public Student{
    protected:
        int external_marks;
};
class University{
    protected:  
        int university_marks;
};
class Result:public Internal,public External,public University{
    public:
        int get_data(){
            std::cout<<"Enter Name, Enrollment number, Internal marks, External marks and University marks :"<<std::endl;
            std::cin>>Internal::name>>Internal::en_no>>internal_marks>>external_marks>>university_marks;
    }
    int display(){
        std::cout<<" Name :"<<Internal::name<<"\n Enrollment number :"<<Internal::en_no<<"\n Internal Marks    :"<<internal_marks<<"\n External Marks    :"<<external_marks<<"\n University Marks  :"<<university_marks;
    }
};
int main(){

    Result r;
    r.get_data();
    r.display();
}
