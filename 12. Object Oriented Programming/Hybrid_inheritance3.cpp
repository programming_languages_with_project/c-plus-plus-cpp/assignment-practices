// write a program to define the followig relationship using hybrid inheritance.

#include<iostream>
using namespace std;

class Student{
    protected:
        char name[20];
        int roll_no;
};
class Test:public Student{
    protected:
        int marks;
        public: 
            int read(){
                cout<<"Enter name,roll number and marks obtained :"<<endl;
                gets(name);
                cin>>roll_no>>marks;
            }
};
class Sports{
    public: 
        int score;
        int read(){
            cout<<"1.Student has won in national sports event\n2.Student has not won in any national sports event\n Enter the your choice :";
            cin>>score;
        }
};
class Result:public Test,public Sports{
    int total;
    public: 
        int calculate(){
            if(score==1)
            total=marks+15;
            else
            total=marks;
        }
        int display(){
            cout<<"The total is "<<total;
        }
};
int main(){

    Result r;
    r.Test::read();
    r.Sports::read();
    r.calculate();
    r.display();
}


