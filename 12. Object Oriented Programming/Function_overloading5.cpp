// Write a program using function overloading to swap 2 integer number and swap 2 float numbers.


#include<iostream>
using namespace std;

int swap(int*a,int*b){
    int temp;
    temp =*a;
    *a = *b;
    *b =temp;
}
int swap(float *a,float *b){
    float temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
int main(){
    int a,b;
    float c,d;
    cout<<"Enter two integer:";
    cin>>a>>b;
    cout<<"Number Entered are: a="<<a<<"and b ="<<b<<endl;
    swap(&a,&b);
    cout<<"After swapping a ="<<a<<"and b ="<<b<<endl;
    cout<<"Enter the float Number:";
    cin>>c>>d;
    cout<<"Numbers entered are c="<<c<<"and d="<<d<<endl;
    swap(&c,&d);
    cout<<"After swapping c="<<c<<"and d="<<d<<endl;
}


