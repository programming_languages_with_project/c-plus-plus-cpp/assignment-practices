// WAP to calculate area of circle and area of rectangle using function overloading.

#include<iostream>
#include<stdio.h>

float area(float r){
    return(3.14*r*r);
}
float area(float l,float b){
    return(l*b);
}
int main(){
    std::cout<<"Area of cicle with radius 10 units is "<<area(10)<<std::endl;
    std::cout<<"Area of rectangle woth sides 5 units and 7 units is "<<area(5,7)<<std::endl;
}