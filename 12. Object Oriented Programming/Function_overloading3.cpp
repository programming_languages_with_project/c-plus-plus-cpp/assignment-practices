// Write a C++ program to generate first 'n' terms of Fibonnaci series using constructor function.

#include<iostream>
using namespace std;

class Fibonacci{
    public:
        Fibonacci(int n){
            int i,a=0,b=1,c;
            cout<<"Fibonacci Series\n"<<a<<"\n"<<b<<"\n";
            for(int i=3;i<=n;i++){
                c = a+b;
                a = b;
                b = c;
                cout<<c<<"\n";
            }
        };
};
int main(){

    int n;
    cout<<"Enter the number of elements of fibonacci series :";
    cin>>n;
    Fibonacci(n);

    return 0;
}



