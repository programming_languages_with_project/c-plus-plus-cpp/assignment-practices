//WAP program to define the relationship using multiple inheritace.


#include<iostream>

class Polygoan{

    protected:
        int height, width;
            public: 
                int read(int a, int b){
                    height = a;
                    width = b;
                }
};
class Output{
    public: 
        int output(int x){
            std::cout<<"Area is "<<x;
        }
};
class Rectangle:public Polygoan,public Output{
    public: 
        int area(){
            return(height*width);
        }
};
class Triangle:public Polygoan,public Output{
    public:
        int area(){
            return(height*width/2);
        }
};
int main(){
    int h,w,choice,a;
    std::cout<<"1. Area of Rectangle \n 2. Area of Triangle \n Enter the choice:"<<std::endl;
    std::cin>>choice;
    std::cout<<"Enter Height and Width :";
    std::cin>>h>>w;
    switch(choice){
        case 1 :
            Rectangle r;
            r.read(h,w);
            a = r.area();
            r.output(a);
            break;

        case 2 :
            Triangle t;
            t.read(h,w);
            a = t.area();
            t.output(a);
            break;
            default: std::cout<<"Invalid choice";    
    }
}





