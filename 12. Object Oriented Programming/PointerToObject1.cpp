// Write a program to demonstrate use of pointer to object.


#include<iostream>
using namespace std;

class Base{
    protected:  
        int a,b;
        public:
            virtual int read(){
                cout<<"Enter two value :";
                cin>>a>>b;
            }
            virtual int display(){
                cout<<"\nThe values are:"<<a<<"\n"<<b<<endl;
            }
};
class Sub:public Base{
    protected:  
        int c,d;
        public: 
            virtual int read(){
                cout<<"Enter 4 values:";
                cin>>a>>b>>c>>d;
            }
            virtual int display(){
                cout<<"\n The values are:"<<a<<"\n"<<b<<"\n"<<c<<"\n"<<d<<endl;
            }
};
int main(){
    Base *ptr;
    Base b;
    Sub s;
    ptr = &b;
    ptr->read();
    ptr->display();
    ptr=&s;
    ptr->read();
    ptr->display();
}




