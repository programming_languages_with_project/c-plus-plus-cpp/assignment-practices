//class Hierarchy:
// Class A,B,C,D has one integer data member p,q,r and s respectively.Write appropiate member function in each class
// to accept and display data. Also write member function in calss D to perform in class D to perform s=p+q+r.


#include<iostream>
using namespace std;

class A{
    public: 
        int p;
        int accept(){
            cout<<"Enter value of p :";
            cin>>p;
        }
        int display(){
            cout<<"p="<<p<<endl;
        }
};
class B:public A{
    protected:  
        int q;
        public: 
            int accept(){
                A::accept();
                cout<<"Enter value of q:";
                cin>>q;
            }
            int display(){
                A::display();
                cout<<"q="<<q<<endl;
            }
};
class C:public A{
    protected:  
        int r;
        public: 
            int accept(){
                cout<<"Enter value of r :";
                cin>>r;
            }
            int display(){
                cout<<"r="<<r<<endl;
            }
};
class D:public B,public C{
    int s;
    public: 
        int accept(){
            B::accept();
            C::accept();
        }
        int display(){
            B::display();
            C::display();
        }
        int calculate(){
            s = B::p+q+r;
            cout<<"s="<<s<<endl;
        }
};
int main(){
    D d;
    d.accept();
    d.display();
    d.calculate();
}





