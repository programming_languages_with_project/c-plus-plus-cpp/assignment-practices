

    // Write a program to add two numbers using single inheritance such that the base class function must accept the two 
    // number form the two numbers form the user and derived class function must add these number and display the sum.

#include<iostream>

class Data{
    protected:
        int a,b;
        public:
            int read(){
                std::cout<<"Enter two Number :"<<std::endl;
                std::cin>>a>>b;
            }
};

class Sum:public Data{
    private:
        int sum;
        public:
            int add(){
                sum = a+b;
            }
            int display(){
                std::cout<<"The sum is_"<<sum;
            }
};
int main(){

    Sum s;
    s.read();
    s.add();
    s.display();
}


