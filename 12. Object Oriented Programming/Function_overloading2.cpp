// Write a C++ program to perform addition of two integer number and three floating point number using function name add() 
// but with different number and types of argrument.

#include<iostream>
using namespace std;

int add(int a,int b){
    int c;
    c = a+b;
    return c;
}
float add(float a,float b, float c){
    float d;
    d = a+b+c;
    return d;
}
int main(){
    int x,a=5,b=6;
    float y,p=3.5,q=6.6,r=2.3;
    x = add(a,b);
    cout<<"Sum = "<<x<<endl;
    y = add(p,q,r);
    cout<<"Sum = "<<y<<endl;
}