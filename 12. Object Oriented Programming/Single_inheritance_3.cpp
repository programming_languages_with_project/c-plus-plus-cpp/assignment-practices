// WAP to implement single inheritance from following accept and display the data for one table.
// [Class - furniture , data member:material,price]------>[Class - Table    data member:height :surface area]


#include<iostream>

class Furniture{
    protected:
        char material[20];
        int price;
};
class Table:public Furniture{
    private:
        int height,area;
        public:
            int accept(){
                std::cout<<"Enter the material,price,height and area :";
                std::cin>>material>>price>>height>>area;
            }
            int display(){
                std::cout<<"Material :"<<material<<"\nPrice "<<price<<"\nHeight "<<height<<"\nArea"<<area;
            }
};
int main(){
    Table t;
    t.accept();
    t.display();
}




