// WAP to demonstrate the use of"this" pointer.

#include<iostream>
using namespace std;

class Compare{
    protected:
        int a;
        public: 
            int read(){
                cout<<"Enter a value:";
                cin>>a;
            }
            Compare compare(Compare c){
                if(a>c.a)
                return *this;
                else
                return c;
            }
            int display(){
                cout<<"The value is :"<<a;
            }
};
int main(){
    Compare c1;
    Compare c2;
    c1.read();
    c2.read();
    Compare c3;
    c3 = c1.compare(c2);
    c3.display();
}

