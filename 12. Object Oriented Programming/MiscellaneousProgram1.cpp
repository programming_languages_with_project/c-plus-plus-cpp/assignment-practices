// WAP to create a two base classes B1 and B2, each of which is publicly inherited by derived class D.

#include<iostream>
using namespace std;

class B1{
    public:
        b1(int n){
            cout<<n<<"\nIn parameterized constructor of class B1\n";
        }
        B1(){
            cout<<"\nIn default constructor of class B1\n";
        }
        ~B1(){
            cout<<"\nIn Destructor of class B1\n";
        }
};
class B2{
    public:
        B2(int n){
            cout<<"\nParameterized Constructor of class B2\n";
        }
        B2(){
            cout<<"\nIn default constructor of class B2\n";
        }
        ~B2(){
            cout<<"\nIn Destructor of class B2\n";
        }
};
class D:public B1,public B2{
    public:
        D(int n){
            cout<<"\nParameterized Constructor of class D\n";
        }
        ~D(){
            cout<<"\nIn Destructor of class D\n";
        }
};
int main(){
    D d(5);
}





