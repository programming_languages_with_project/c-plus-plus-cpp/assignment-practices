// Write a program to demonstrate pure virtual function.

#include<iostream>
using namespace std;

class Base{
    protected:
        int a,b;
        public:
            virtual int read(){

            }
            virtual int display()=0;

};
class Sub:public Base{
    protected:  
        int c,d;
        public:
            int read(){
                cout<<"Enter 4 value:";
                cin>>a>>b>>c>>d;
            }
            int display(){
                cout<<"The value are:"<<a<<"\n"<<b<<"\n"<<c<<"\n"<<d;
            }
};
int main(){
    Sub s;
    Base *ptr;
    ptr = &s;
    ptr -> read();
    ptr ->display();
}




