// WAP to find a area of a circle using single inheritance such a that the base class function must accept the radius 
// from the user and derived class function must calculate and display the area. 


#include<iostream>

class Data{
    protected:
        int r;
        public:
        int read(){
            std::cout<<"Enter the radius";
            std::cin>>r;    
        }
};
class Area:public Data{
    private:
        float result;
        public:
            void compute(){
                result = 3.14*r*r;
            }
            int display(){
                std::cout<<"The area of the circle is "<<result;
            }
};
int main(){

    Area a;
    a.read();
    a.compute();
    a.display();
}




