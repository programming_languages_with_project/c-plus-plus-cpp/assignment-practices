// WAP to calculate the area* of triangle,reactangle and circle using constructor overloading.The program should be menu-driven.
/*
#include<iostream>
#include<math.h>
using namespace std;

class Area{
    float area;
    public: 
        Area(float r){
            area = 3.14*r*r;
        }
        Area(float l,float b){
            area = l*b;
        }
        Area(float a,float b, float c){
            float s;
            s =(a+b+c)/2;
            area = (s*(s-a)*(s-b)*(s-c));
            area = pow(area,0.5);
        }
        int display(){
            cout<<"Area ="<<area;
        }
};
int main(){
    int choice;
    float x,y,z;
    cout<<"1.Area of Circle\n2.Area of Reactangle\n3.Area of triangle\nEnter your choice:";
    cin>>choice;
    switch(choice){
        case 1: 
            cout<<"Enter the radius of the circle";
            cin>>x;
            Area a(x);
            a.display();
            break;
        case 2: 
            cout<<"Enter the length and breadth of the rectangle";
            cin>>x>>y;
            Area a1(x,y);
            a1.display();
            break;
        case 3: 
            cout<<"Enter the length of the three sides of the triangle.";
            cin>>x>>y>>z;
            Area a2(x,y,z);
            a2.display();
            break;
        default:
            cout<<"Invalid choice:";    
    }
    return 0;
}

*/

#include<iostream>
#include<cmath>
using namespace std;

class Area {
    float area;
public:
    // Constructor for the area of a circle
    Area(float r) {
        area = 3.14 * r * r;
    }

    // Constructor for the area of a rectangle
    Area(float l, float b) {
        area = l * b;
    }

    // Constructor for the area of a triangle using Heron's formula
    Area(float a, float b, float c) {
        float s = (a + b + c) / 2;
        area = sqrt(s * (s - a) * (s - b) * (s - c));
    }

    // Function to display the area
    void display() {
        cout << "Area = " << area << endl;
    }
};

int main() {
    int choice;
    float x, y, z;

    cout << "1. Area of Circle\n2. Area of Rectangle\n3. Area of Triangle\nEnter your choice: ";
    cin >> choice;

    switch (choice) {
        case 1:
            cout << "Enter the radius of the circle: ";
            cin >> x;
            Area a(x);
            a.display();
            break;

        case 2:
            cout << "Enter the length and breadth of the rectangle: ";
            cin >> x >> y;
            Area a1(x, y);
            a1.display();
            break;

        case 3:
            cout << "Enter the lengths of the three sides of the triangle: ";
            cin >> x >> y >> z;
            Area a2(x, y, z);
            a2.display();
            break;

        default:
            cout << "Invalid choice." << endl;
    }

    return 0;
}



