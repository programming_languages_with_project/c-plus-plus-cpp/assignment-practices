// Write a program to find how many object of a class has been created using static member function.

#include<iostream>
using namespace std;
class Count{
    private:
        static
        int counter;
        public:
            Count(){
                counter++;
            }
            static int display(){
                return counter;
            }
};
int Count::counter=0;
int main(){
    Count c1;
    cout<<"Number of Objects:"<<c1.display()<<endl;
    Count c2;
    Count c3;
    cout<<"Number of Objects:"<<c1.display()<<endl;
}




