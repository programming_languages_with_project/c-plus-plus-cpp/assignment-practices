

#include<iostream>
using namespace std;

class Complex{
    int x,y;
    public: 
        int read(){
            cout<<"Enter real and imaginary parts of a complex number:";
            cin>>x>>y;
        }
        Complex(){

        }
        Complex(int a,int b){
            x = a;
            y = b;
        }
        Complex operator +(Complex c){
            return Complex(x+c.x,y+c.y);
        }
        int display(){
            if(y<0)
            cout<<x<<y<<"i";
            else 
            cout<<x<<"+i"<<y;
        }
};
int main(){
    Complex c1;
    Complex c2;
    Complex c3;
    c1.read();
    c2.read();
    c3 = c1 + c2;
    c3.display();
}


