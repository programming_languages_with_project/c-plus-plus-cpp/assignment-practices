// Write a program to prepare the marksheet of the collage examination with the following items read from the keyboard.
//Name of the student,roll_no,subject name,subject code,internal marks,external marks.Design the base class consisting of data members such as name of student,rollno,subject name.
//The derived class consists of the data members,viz,subject code,internal marks and external marks.

#include<iostream>
using namespace std;

class Base{
    protected:
        char name[20],subject[20];
        int roll;
};
class Marks:public Base{
    int code,internal,external;
    public:
        int accept(){
            cout<<"Enter name,subject,roll number,code,internal marks,external marks";
            gets(name);
            gets(subject);
            cin>>roll>>code>>internal>>external;
        }
        int display(){
            cout<<name<<"\t"<<subject<<"\t"<<roll<<"\t"<<code<<"\t"<<internal<<"\t"<<external<<endl;
        }
};
int main(){
    int i,n;
    Marks m[100];
    cout<<"Enter value of n:";
    cin>>n;
    for(int i=0;i<n-1;i++){
        m[i].accept();
    }
    cout<<"Name\tSubject\tRoll no\tCode\tInternal\tExternal\n";
    for(int i=0;i<n-1;i++){
        m[i].display();
    }
}


