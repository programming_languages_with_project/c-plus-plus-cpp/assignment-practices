// Write a program to add private members variables of two classes using friend function.

#include<iostream>
using namespace std;

class B;
class A{
    protected:  
        int a;
        public:
            A(){
                a =5;
            }
            friend int add(A*a,B*b);
};
class B{
    protected:  
        int b;
        public: 
            B(){
                b=10;
            }
            friend int add(A*a,B*b);
};
int add(A *p1,B *p2){
    int sum =0;
    sum = p1->a + p2->b;
    cout<<"Sum = "<<sum;
}
int main(){
    A a1;
    B b1;
    add(&a1,&b1);
}








