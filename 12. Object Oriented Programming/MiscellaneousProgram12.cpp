//Declare a class logic_gate to represent logic gates. The class has three data members -input1,input2,input3 to represent three inputs to the logic gates.
//The class also has virtual function member called get_gate_output.Derive two classes from the base class logic_gate,namely,and_gate and or_gate to represent'logical and 
//gate'and 'logical and gate' repectively.Define function get_gate_output in both of these classes to get the output of the gate.
//Show use of above classes and function to demonstrate dyanamic polymorphism in function main.

#include<iostream>
using namespace std;

class logic_gate{
    public: 
        int input1,input2,input3;
        int get_input(){
            cout<<"Enter the inputs :";
            cin>>input1>>input2>>input3;
        }
        virtual int get_gate_output()=0;
};
class and_gate:public logic_gate{
    public: 
        virtual int get_gate_output(){
            return(input1 & input2 & input3);
        }
};
class or_gate:public logic_gate{
    public: 
        virtual int get_gate_output(){
            return(input1|input2|input3);
        }
};
int main(){
    int o1;
    logic_gate *g;
    and_gate a;
    or_gate o;
    
    g= &a;
    g->get_input();
    o1=g->get_gate_output();
    cout<<"Output of AND gate:"<<o1<<endl;

    g=&o;
    g->get_input();
    o1=g->get_gate_output();
    cout<<"Output of OR gate:"<<o1<<endl;
}




