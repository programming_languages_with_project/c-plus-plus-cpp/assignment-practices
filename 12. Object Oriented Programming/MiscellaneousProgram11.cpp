//create a class student that stores roll_no,name.Create a class test that stores marks obtained in five subject.
// Class result derived from student and test contains the total marks and percentage obtained in test.
// Input and display information of a student.


#include<iostream>
using namespace std;

class Student{
    protected:  
        int roll_no;
        char name[20];
};
class Test{
    protected:  
        int s1,s2,s3,s4,s5;
};
class Result:public Student,public Test{
    int total;
    float percentage;
    public: 
        int get_data(){
            cout<<"Enter name,roll number and marks in 5 subject:";
            cin>>name>>roll_no>>s1>>s2>>s3>>s4>>s5;
        }
        int calculate(){
            total = s1+s2+s3+s4+s5;
            percentage = total/5.0;
        }
        int display(){
            cout<<"Name:"<<name<<"\nRoll no."<<roll_no<<"\nTotal :"<<total<<"\nPercentage :"<<percentage;
        }
};
int main(){
    Result r;
    r.get_data();
    r.calculate();
    r.display();

}





