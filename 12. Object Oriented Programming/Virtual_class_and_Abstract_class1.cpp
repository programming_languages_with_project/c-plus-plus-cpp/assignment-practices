// Write a program to demonstrate virtual class or abstract class.

#include<iostream>
using namespace std;

class Base{
    protected:  
        int a,b;
        public: 
            int read(){
                cout<<"Enter two value:";
                cin>>a>>b;
            }
            int display(){
                cout<<"The values are:"<<a<<"\n"<<b;
            }
};
class Sub: public virtual Base{
    protected:  
        int c,d;
            public: 
                int read(){
                    cout<<"Enter 4 value: ";
                    cin>>a>>b>>c>>d;
                }
                int display(){
                    cout<<"The value are:"<<a<<"\n"<<b<<"\n"<<c<<"\n"<<d;
                }
};
int main(){
    Sub s;
    s.read();
    s.display();
}





