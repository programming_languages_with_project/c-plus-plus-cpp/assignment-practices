//Assume that vehicle class is define as base class with price and year of manufacturing.Drive two classes namely bus and truck form base class with bus with
// seating capacity and truck with loading capacity.Develop classes with necessary member function to get and put data.Demonstrate its use in main().


#include<iostream>
using namespace std;

class Vehicle{
    protected:  
        float price;
        int year;
};
class Bus:public Vehicle{
    int seats;
    public: 
        void get_data(){
            cout<<"Enter price,year and seats:";
            cin>>price>>year>>seats;
        }
        int put_data(){
            cout<<"Bus:\nPrice="<<price<<endl<<"Year"<<year<<endl<<"Seats="<<seats<<endl;
        }
};
class Truck:public Vehicle{
    int load;
    public:
        int get_data(){
            cout<<"Enter price,year nd load:";
            cin>>price>>year>>load;
        }
        int put_data(){
            cout<<"Truck:\nPrice="<<price<<endl<<"Year="<<year<<endl<<"Load="<<load<<endl;
        }
};
int main(){
    Bus b;
    b.get_data();
    b.put_data();
    Truck t;
    t.get_data();
    t.put_data();
}






