

/// Write a program to define a class having data member principle,duration and rate-of-interest.
/// Declare rate-of-interest as static member variable .Calculate the simple interest and display it for one object.


    #include<iostream>
    #include<stdio.h>
    using namespace std;

    class Interest{
        private:
            static float rate;
            float principle,duration,sim_int;
            public:
                int accept(){
                    cout<<"Enter Principle amount and duration:";
                    cin>>principle>>duration;
                }
                void calculate(){
                    sim_int = principle*rate*duration/100;
                }
                void display(){
                    cout<<"Simple Interest="<<sim_int;
                }
    };
    float Interest::rate = 10;
    int main(){
        Interest i;
        i.accept();
        i.calculate();
        i.display();
    }
